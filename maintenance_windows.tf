resource "aws_ssm_maintenance_window" "patching_window_live" {
  name              = "patch-install-live"
  enabled           = var.patching_enabled_live
  schedule_timezone = var.schedule_timezone
  schedule          = var.patching_window_live
  duration          = var.patching_duration
  cutoff            = var.patching_cutoff
}

resource "aws_ssm_maintenance_window" "patching_window_testbed" {
  name              = "patch-install-testbed"
  enabled           = var.patching_enabled_testbed
  schedule_timezone = var.schedule_timezone
  schedule          = var.patching_window_testbed
  duration          = var.patching_duration
  cutoff            = var.patching_cutoff
}

resource "aws_ssm_maintenance_window" "patching_window_linux" {
  count             = var.linux_enabled ? 1 : 0
  name              = "patch-install-linux"
  enabled           = var.patching_enabled_linux
  schedule_timezone = var.schedule_timezone
  schedule          = var.patching_window_live
  duration          = var.patching_duration
  cutoff            = var.patching_cutoff
}
