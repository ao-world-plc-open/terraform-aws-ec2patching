resource "aws_ssm_maintenance_window_target" "patching_target_live" {
  count         = length(var.patch_group_live)
  window_id     = aws_ssm_maintenance_window.patching_window_live.id
  resource_type = "INSTANCE"

  targets {
    key    = "tag:Patch Group"
    values = [element(var.patch_group_live, count.index)]
  }
}

resource "aws_ssm_maintenance_window_target" "patching_target_testbed" {
  count         = length(var.patch_group_testbed)
  window_id     = aws_ssm_maintenance_window.patching_window_testbed.id
  resource_type = "INSTANCE"

  targets {
    key    = "tag:Patch Group"
    values = [element(var.patch_group_testbed, count.index)]
  }

}

resource "aws_ssm_maintenance_window_target" "patching_target_linux" {
  count         = var.linux_enabled ? 1 : 0
  window_id     = aws_ssm_maintenance_window.patching_window_linux[count.index].id
  resource_type = "INSTANCE"

  targets {
    key    = "tag:Patch Group"
    values = [element(var.patch_group_linux, count.index)]
  }
}
