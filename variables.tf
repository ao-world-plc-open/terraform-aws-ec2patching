variable "approval_date" {
  description = "How many days after patch Tuesday until approved"
  type        = number
  default     = 6
}

variable "concurrency_live" {
  description = "The maximum amount of concurrent instances of a task that will be executed in parallel"
  type        = number
  default     = 1
}

variable "concurrency_testbed" {
  description = "The maximum amount of concurrent instances of a task that will be executed in parallel"
  type        = number
  default     = 20
}

variable "custom_tags" {
  description = "A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence"
  type        = map(string)
  default     = {}
}

variable "linux_enabled" {
  description = "Enable patching of Amazon Linux 2, controls creation of resources via `count`"
  type        = bool
  default     = false
}

variable "operating_system_linux" {
  description = "Currently, only supporting Amazon Linux 2"
  default     = "AMAZON_LINUX_2"
  type        = string
}

variable "patch_classification" {
  description = "The list of patch classifications defined for updates"
  type        = list(string)
  default     = ["CriticalUpdates", "DefinitionUpdates", "SecurityUpdates", "Drivers", "UpdateRollups"]
}

variable "patching_cutoff" {
  description = "The number of hours before the end of the Maintenance Window that Systems Manager stops scheduling new tasks for execution"
  type        = number
  default     = 0
}

variable "patching_duration" {
  description = "The duration of the Maintenance Window in hours"
  type        = number
  default     = 5
}

variable "patching_enabled_linux" {
  description = "Whether the maintenance window is enabled or not, set to false to pause the window but not remove the resources"
  type        = bool
  default     = true
}

variable "patching_enabled_live" {
  description = "Whether the maintenance window is enabled or not, set to false to pause the window but not remove the resources"
  type        = bool
  default     = true
}

variable "patching_enabled_testbed" {
  description = "Whether the maintenance window is enabled or not, set to false to pause the window but not remove the resources"
  type        = bool
  default     = true
}

variable "patch_group_linux" {
  description = "Patch group to target Amazon Linux 2 instances"
  type        = list(string)
  default     = ["Linux"]
}

variable "patch_group_live" {
  description = "Patch group to target live Windows instances"
  type        = list(string)
  default     = ["Live"]
}

variable "patch_group_testbed" {
  description = "Patch group to target test bed Windows instances"
  type        = list(string)
  default     = ["Test Bed"]
}

variable "patch_severity" {
  description = "Severities of updates we accept for Windows"
  type        = list(string)
  default     = ["Critical", "Important", "Moderate", "Low", "Unspecified"]
}

variable "patch_severity_linux" {
  description = "Severities of updates we accept for Amazon Linux 2"
  type        = list(string)
  default     = ["Critical", "Important", "Medium", "Low"]
}

variable "product_versions" {
  description = "List of Windows versions we support for patching"
  type        = list(string)
  default     = ["WindowsServer2008", "WindowsServer2008R2", "WindowsServer2012", "WindowsServer2012R2", "WindowsServer2016", "WindowsServer2019"]
}

variable "patching_window_linux" {
  description = "The schedule to patch Amazon Linux 2 instances on. See the [cron expression docs](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-create-rule-schedule.html#eb-cron-expressions) for more info"
  type        = string
  default     = "cron(0 0 00 ? 1/1 TUE#4 *)"
}

variable "patching_window_live" {
  description = "The schedule to patch live Windows instances on. See the [cron expression docs](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-create-rule-schedule.html#eb-cron-expressions) for more info"
  type        = string
  default     = "cron(0 0 00 ? 1/1 TUE#4 *)"
}

variable "patching_window_testbed" {
  description = "The schedule to patch test bed Windows instances on. See the [cron expression docs](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-create-rule-schedule.html#eb-cron-expressions) for more info"
  type        = string
  default     = "cron(0 0 00 ? 1/1 TUE#3 *)"
}

variable "schedule_timezone" {
  description = "Time Zone on whihch the patching schedule should be run"
  type        = string
  default     = "Europe/London"
}
