resource "aws_ssm_maintenance_window_task" "patching_task_live" {
  name             = "patching_install_live"
  description      = "Task to install patches for instances tagged as Patch Group: Live"
  window_id        = aws_ssm_maintenance_window.patching_window_live.id
  task_type        = "RUN_COMMAND"
  task_arn         = "AWS-RunPatchBaseline"
  priority         = 1
  service_role_arn = aws_iam_role.SSMMaintenanceWindowRole.arn
  max_concurrency  = var.concurrency_live
  max_errors       = 1

  targets {
    key    = "WindowTargetIds"
    values = aws_ssm_maintenance_window_target.patching_target_live.*.id
  }

  task_invocation_parameters {
    run_command_parameters {
      parameter {
        name   = "Operation"
        values = ["Install"]
      }
    }
  }
}

resource "aws_ssm_maintenance_window_task" "patching_task_testbed" {
  name             = "patching_install_testbed"
  description      = "Task to install patches for instances tagged as Patch Group: Test Bed"
  window_id        = aws_ssm_maintenance_window.patching_window_testbed.id
  task_type        = "RUN_COMMAND"
  task_arn         = "AWS-RunPatchBaseline"
  priority         = 1
  service_role_arn = aws_iam_role.SSMMaintenanceWindowRole.arn
  max_concurrency  = var.concurrency_testbed
  max_errors       = 1

  targets {
    key    = "WindowTargetIds"
    values = aws_ssm_maintenance_window_target.patching_target_testbed.*.id
  }

  task_invocation_parameters {
    run_command_parameters {
      parameter {
        name   = "Operation"
        values = ["Install"]
      }
    }
  }
}
resource "aws_ssm_maintenance_window_task" "patching_task_linux" {
  count            = var.linux_enabled ? 1 : 0
  name             = "patching_install_linux"
  description      = "Task to install patches for instances tagged as Patch Group: Linux"
  window_id        = aws_ssm_maintenance_window.patching_window_linux[count.index].id
  task_type        = "RUN_COMMAND"
  task_arn         = "AWS-RunPatchBaseline"
  priority         = 2
  service_role_arn = aws_iam_role.SSMMaintenanceWindowRole.arn
  max_concurrency  = var.concurrency_live
  max_errors       = 1

  targets {
    key    = "WindowTargetIds"
    values = aws_ssm_maintenance_window_target.patching_target_linux.*.id
  }

  task_invocation_parameters {
    run_command_parameters {
      parameter {
        name   = "Operation"
        values = ["Install"]
      }
    }
  }
}