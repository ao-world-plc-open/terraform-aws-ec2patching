# Terraform Module: AWS EC2 Patching

This module is used to patch your Windows and/or Amazon Linux 2 instances.
Patches are set to be released 6 days after Microsoft Patch Tuesday (2nd 
Tuesday of the month).

The module does not setup scanning, as that's deployed by default to accounts
via CloudFormation at the Organisation level.

* [Example Usage](#example-usage)
  * [Pre-Requisites](#pre-requisites)
  * [Basic](#basic)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Pre-Requisites

Firstly the IAM Role assigned to the instances you want to patch **must**
have the `AmazonSSMManagedInstanceCore` policy attached to it for everything
to work. If you don't have a role already, please create one and attach the
role.

If the instances belong to an Auto Scaling Group (ASG) you should add these
scan only tags instead. Otherwise when SSM shuts the instance down, the ASG
will think the instance has failed and recreate it.

```
Key: "Patch Group", Value: "Scan Only"
```

For non-ASG instances to be patched they must be tagged with one of the
following (if using the module defaults):

```
# Live/Production
Key: "Patch Group", Value: "Live"
# QA/Staging/Dev
Key: "Patch Group", Value: "Test Bed"
# Amazon Linux 2
Key: "Patch Group", Value "Linux"
```

### Basic

Deploying the module is as simple as this, no inputs are required as sane
defaults have already been added. This will add maintenance windows of the
following:

```
# Live/Production
4th Tuesday of the Month
# QA/Staging/Dev
3rd Tuesday of the Month
# Amazon Linux 2
4th Tuesday of the Month
```

```hcl
module "patching" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ec2patching.git?ref=v5.1.0"
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| approval\_date | How many days after patch Tuesday until approved | `number` | `6` | no |
| concurrency\_live | The maximum amount of concurrent instances of a task that will be executed in parallel | `number` | `1` | no |
| concurrency\_testbed | The maximum amount of concurrent instances of a task that will be executed in parallel | `number` | `20` | no |
| custom\_tags | A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence | `map(string)` | `{}` | no |
| linux\_enabled | Enable patching of Amazon Linux 2, controls creation of resources via `count` | `bool` | `false` | no |
| operating\_system\_linux | Currently, only supporting Amazon Linux 2 | `string` | `"AMAZON_LINUX_2"` | no |
| patch\_classification | The list of patch classifications defined for updates | `list(string)` | ```[ "CriticalUpdates", "DefinitionUpdates", "SecurityUpdates", "Drivers", "UpdateRollups" ]``` | no |
| patch\_group\_linux | Patch group to target Amazon Linux 2 instances | `list(string)` | ```[ "Linux" ]``` | no |
| patch\_group\_live | Patch group to target live Windows instances | `list(string)` | ```[ "Live" ]``` | no |
| patch\_group\_testbed | Patch group to target test bed Windows instances | `list(string)` | ```[ "Test Bed" ]``` | no |
| patch\_severity | Severities of updates we accept for Windows | `list(string)` | ```[ "Critical", "Important", "Moderate", "Low", "Unspecified" ]``` | no |
| patch\_severity\_linux | Severities of updates we accept for Amazon Linux 2 | `list(string)` | ```[ "Critical", "Important", "Medium", "Low" ]``` | no |
| patching\_cutoff | The number of hours before the end of the Maintenance Window that Systems Manager stops scheduling new tasks for execution | `number` | `0` | no |
| patching\_duration | The duration of the Maintenance Window in hours | `number` | `5` | no |
| patching\_enabled\_linux | Whether the maintenance window is enabled or not, set to false to pause the window but not remove the resources | `bool` | `true` | no |
| patching\_enabled\_live | Whether the maintenance window is enabled or not, set to false to pause the window but not remove the resources | `bool` | `true` | no |
| patching\_enabled\_testbed | Whether the maintenance window is enabled or not, set to false to pause the window but not remove the resources | `bool` | `true` | no |
| patching\_window\_linux | The schedule to patch Amazon Linux 2 instances on. See the [cron expression docs](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-create-rule-schedule.html#eb-cron-expressions) for more info | `string` | `"cron(0 0 00 ? 1/1 TUE#4 *)"` | no |
| patching\_window\_live | The schedule to patch live Windows instances on. See the [cron expression docs](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-create-rule-schedule.html#eb-cron-expressions) for more info | `string` | `"cron(0 0 00 ? 1/1 TUE#4 *)"` | no |
| patching\_window\_testbed | The schedule to patch test bed Windows instances on. See the [cron expression docs](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-create-rule-schedule.html#eb-cron-expressions) for more info | `string` | `"cron(0 0 00 ? 1/1 TUE#3 *)"` | no |
| product\_versions | List of Windows versions we support for patching | `list(string)` | ```[ "WindowsServer2008", "WindowsServer2008R2", "WindowsServer2012", "WindowsServer2012R2", "WindowsServer2016", "WindowsServer2019" ]``` | no |
| schedule\_timezone | Time Zone on whihch the patching schedule should be run | `string` | `"Europe/London"` | no |

## Outputs

No outputs.

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.